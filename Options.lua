local GlobalFontEditor = LibStub("AceAddon-3.0"):GetAddon("GlobalFontEditor")
local LSM    = LibStub:GetLibrary("LibSharedMedia-3.0")

GlobalFontEditor.fontDefaults = {}

function GlobalFontEditor:InitializeConfiguration()
	self.defaultConfig	= {
		profile = {
			fonts = self.fontDefaults
		}
	}	
	
	self.db = LibStub("AceDB-3.0"):New("GlobalFontEditorDB", self.defaultConfig, true)
	
	self:RegisterChatCommand("GlobalFontEditor", "ChatCommand")
	self:RegisterChatCommand("GFE", "ChatCommand")
end

function GlobalFontEditor:InitializeConfigurationGUI()
	self.Configuration = { }	
	self.Configuration.Main = {
		name = "GlobalFontEditor", 
		handler = GlobalFontEditor, 
		type = "group", 
		get = "GetValue", 
		set = "SetValue",
		args = {}
	}
	
	self:InitializeFontsConfiguration(self.fonts, self.Configuration.Main)	
	
	self.Configuration.Profiles = LibStub("AceDBOptions-3.0"):GetOptionsTable(self.db)
	self.Configuration.Profiles.name = "Profiles"
	
	self.Configuration.FontIdentifier = {
		name = "Font Identifier",
		type = "group",
		args = {
			["Description1"] = {
				type = "description",
				fontSize = "medium",
				order = 1,
				name =  
[[        Enabling this tool will open a tooltip on your UI which allows you to identify many of the fonts 
        that you see on your screen.  Once your've enabled it, just move your mouse cursor to a part of the
        screen that contains some text, and the tooltip will update with information about it's font.
]]
			},
			["Description2"] = {
				type = "description",
				fontSize = "large",
				order = 2,
				name =
[[|cffff0000Warning:|r
]]
			},
			["Description3"] = {
				type = "description",
				fontSize = "medium",
				order = 3,
				name =
[[        |cffff8800The font identifier tool uses a lot of CPU power to run.  Do not leave it enabled when you
        are not in the process of using it.  It can take up to half a second for the tooltip to
        refresh sometimes, so try to keep the cursor in the same spot for at least that long if you
        are having difficulty getting information to appear.
]]
			},
			["Description4"] = {
				type = "description",
				fontSize = "medium",
				order = 4,
				name =
[[        |cffff8800Font identifier should reveal information about all fonts used in the default blizzard UI.
        Many addons do not register their FontStrings globally and as a result those will not show
        up when moused over.
]]
			},
			["Description5"] = {
				type = "description",
				fontSize = "large",
				order = 5,
				name = [[|cffffdc42The tooltip shows the following information:|r
]]
			},
			["Description6"] = {
				type = "description",
				fontSize = "medium",
				order = 6,
				name =
[[    |cff0080ffName: e.g. ActionButton1Hotkey|r

        This is the global name of the particular text you are moused over right now.  You can access 
        the FontString object for that piece of text from the global environment by using the name given.
        You could type the following into a chat window, "/run print(ActionButton1Hotkey:GetText())".
        That command would print the text of ActionButton1Hotkey to your first chat window tab.
]]
			},
			["Description7"] = {
				type = "description",
				fontSize = "medium",
				order = 7,
				name =
[[    |cff0080ffFont: e.g. Friz Quadrata TT|r

        This is the name of the font family of your moused over text.  By default this shows the key
        name of this font in LibSharedMedia.  If you don't have LibSharedMedia for some reason, or the
        font you are examining isn't registered then it will simply show the path to the .ttf file.
]]
			},
			["Description8"] = {
				type = "description",
				fontSize = "medium",
				order = 8,
				name =
[[    |cff0080ffParent: e.g. GameFontNormal|r

        This is the Font object that the moused over text inherits from.  Normally, changes made to
        a Font's parent will cascade downward to all of it's children.  Changes made directly to
        a Font will override changes inherited from parents.
]]
			},
			["Description9"] = {
				type = "description",
				fontSize = "medium",
				order = 9,
				name =
[[    |cff0080ffRoot: e.g. SystemFont_Shadow_Small|r
        
        This is the Font object that is at the top of the inheritance hierarchy of the moused over 
        text.  For example, GameFontHighlightSmall inherits GameFontNormalSmall, and GameFontNormalSmall
        inherits SystemFont_Shadow_Small.  GameFontHighlightSmall's root is SystemFont_Shadow_Small.
        This is useful to know so that you can easily find the correct section on the Global Font Editor
        settings page to allow you to make changes to the moused over text.
]]
			},
			["ToggleButton"] = {
				name = "Font Identifier",
				type = "execute",
				func = function() GFE:FontStringIdentTooltip_Toggle() end,
				order = 10
			},
			["Padding"] = {
				type = "description",
				name = "      ",
				fontSize = "medium",
				order = 11
			},
			["StatusLabel"] = {
				fontSize = "medium",
				type = "description",
				name = function() 
							return FontStringIdentTooltip:IsVisible() and 
								[[Font Identifier: |cffff0000Enabled]] or 
								[[Font Identifier: |cff00ff00Disabled]] 
						end
			}
		}
	}	

	LibStub("AceConfig-3.0"):RegisterOptionsTable("GlobalFontEditor",          self.Configuration.Main)
	LibStub("AceConfig-3.0"):RegisterOptionsTable("GlobalFontEditor.Profiles", self.Configuration.Profiles)
	LibStub("AceConfig-3.0"):RegisterOptionsTable("GlobalFontEditor.FontIdentifier", self.Configuration.FontIdentifier)

	self.ConfigurationFrame = 
	LibStub("AceConfigDialog-3.0"):AddToBlizOptions("GlobalFontEditor", self.Configuration.Main.name)	
	LibStub("AceConfigDialog-3.0"):AddToBlizOptions("GlobalFontEditor.FontIdentifier", self.Configuration.FontIdentifier.name, "GlobalFontEditor")
	LibStub("AceConfigDialog-3.0"):AddToBlizOptions("GlobalFontEditor.Profiles", self.Configuration.Profiles.name, "GlobalFontEditor")

	self.ConfigurationFrame.okay   = function() self:Update() end
	self.ConfigurationFrame.cancel = function() self:OnCloseConfigurationFrame() end
	
	self.db.RegisterCallback(self, "OnProfileChanged", "Update")
	self.db.RegisterCallback(self, "OnProfileCopied",  "Update")
	self.db.RegisterCallback(self, "OnProfileReset",   "Reset")
end

--- Recursively create config groups for all fonts
--- arg 'recurse' tracks the ordering of items to preserve alphabetical order
function GlobalFontEditor:InitializeFontsConfiguration(fonts, group, recurse)
	local iorder = recurse or 1
	for i = 1, #fonts do	
		k,v = next(fonts[i])			
		
		self:InitializePreviewWidget(_G[k])		
		
		group.args[k] = {			
			name = k,			
			width = "double",
			type = "group",
			order = iorder,			
			args = self:InitializeFontGroup(k, iorder + 1)
		}
		
		iorder = iorder + 9
		
		if recurse then group.args[k].inline = true end
	
		if type(v) == "table" then
			iorder = self:InitializeFontsConfiguration(self.keySort(v), group.args[k], iorder)
		end
		
		iorder = iorder + 1
	end
	
	return iorder;
end

--- Create the configuration group for the provided font
--- If argument 'group' is set, it will modify it instead of creating a new table
function GlobalFontEditor:InitializeFontGroup(font, iorder, group)
	local group = group or {}
	
	group[font.."_NAME"] = {
		name = "Font",
		dialogControl = 'LSM30_Font',
		values = LSM:HashTable("font"),			
		type   = "select",
		order = iorder
	}
	
	group[font.."_SIZE"] = {
		name = "Size",					
		type   = "range",
		order = iorder + 1,
		min = 1,
		max = 32,
		step = 1,
		bigStep = 5
	}
	
	group[font.."_OUTLINE"] = {
		name = "Outline",
		type = "toggle",		
		order = iorder + 2
	}
	
	group[font.."_THICKOUTLINE"] = {
		name = "ThickOutline",
		type = "toggle",		
		order = iorder + 3
	}
	
	group[font.."_MONOCHROME"] = {
		name = "Monochrome",
		type = "toggle",		
		order = iorder + 4
	}
	
	group[font.."_RESET"] = {
		name = "Reset",					
		type   = "execute",
		order = iorder + 5,
		func = function() self:ResetFont(font, iorder, group) end,					
	}	
	
	group[font.."_HEADER2"] = {
		name = "",
		type = "header",
		order = iorder + 6
	}
	
	group[font.."_PREVIEW"] = {
		name = font.."Preview",
		type = "group",	
		inline = true,
		order = iorder + 7,
		args = {
			[font.."_PREVIEW_WIDGET"] = {
				name = font.."PreviewWidget",
				type = "input",
				width = "full",
				dialogControl = "FontPreview-"..font				
			}
		}
	}
	
	return group
end

function GlobalFontEditor:GetValue(info)
	local dbFont = self.db.profile.fonts[info[#info-1]]
	local curOpt = info.option.name

	if dbFont then		
		if curOpt == "Font" then
			return dbFont.font
		elseif curOpt == "Size" then
			return dbFont.size
		elseif curOpt == "Outline" then
			return dbFont.outline
		elseif curOpt == "ThickOutline" then
			return dbFont.thickoutline
		elseif curOpt == "Monochrome" then
			return dbFont.monochrome
		end
	end
end

--- The setter checks for missing values in the db and replaces those with the current values of 
--- the font that is being modified
function GlobalFontEditor:SetValue(info, value)	
	self.db.profile.fonts[info[#info-1]] = self.db.profile.fonts[info[#info-1]] or {}
	local dbFont = self.db.profile.fonts[info[#info-1]]
	local curOpt = info.option.name
	
	if curOpt == "Font" then
		dbFont.font = value
	elseif curOpt == "Size" then
		dbFont.size = value
	elseif curOpt == "Outline" then
		dbFont.outline = value
	elseif curOpt == "ThickOutline" then
		dbFont.thickoutline = value
	elseif curOpt == "Monochrome" then
		dbFont.monochrome = value
	end
	
	if self:IsWidgetType(_G[info[#info-1]], "Font") then	
		local kfont,ksize,kflags = _G[info[#info-1]]:GetFont()
		local font = LSM:Fetch(LSM.MediaType.FONT, dbFont.font, true) or kfont
		local size = dbFont.size or ksize
		local flags = self:ConcatFlags{
		   dbFont.outline and "OUTLINE" or dbFont.outline == nil and strfind(kflags, "OUTLINE") and "OUTLINE",
		   dbFont.thickoutline and "THICKOUTLINE" or dbFont.thickoutline == nil and strfind(kflags, "THICKOUTLINE") and "THICKOUTLINE",
		   dbFont.monochrome and "MONOCHROME" or dbFont.monochrome == nil and strfind(kflags, "MONOCHROME") and "MONOCHROME"
		}
	
		ChatFrame1:AddMessage(font)
		ChatFrame1:AddMessage(size)
		ChatFrame1:AddMessage(flags)
		_G[info[#info-1]]:SetFont(font, size, flags)
	end
end

--- Saves all of the current system fonts' properties for later retrieval
--- Note: this needs to run before any other addon (including this one...) 
--- 	  starts to modify any of the system fonts.
--- Also: If this runs before fonts are registered with LSM, then the font name
---		  saved will be the raw font file.  This needs to be updated later with
---		  a call to GlobalFontEditor:LoadLSMKeysForDefault()
function GlobalFontEditor:LoadSystemDefaults(fonts)	
	for i = 1, #fonts do	
		k,v = next(fonts[i])		
		if self:IsWidgetType(_G[k], "Font") then
			local f,s,flraw = _G[k]:GetFont()
			flraw = flraw and {strsplit(",", flraw)}
			local fl = {}
			for k,v in pairs(flraw) do fl[v] = v; fl[k] = nil; end
			
			self.fontDefaults[k] = {}
			self.fontDefaults[k].font = LSM:GetKey(LSM.MediaType.FONT, f) or f
			self.fontDefaults[k].size = s
			self.fontDefaults[k].outline = fl.OUTLINE and true or false
			self.fontDefaults[k].thickoutline = fl.THICKOUTLINE and true or false
			self.fontDefaults[k].monochrome = fl.MONOCHROME and true or false
		end
				
		if type(v) == "table" then
			self:LoadSystemDefaults(self.keySort(v))
		end
	end
end

--- Take all saved font values and apply them
--- If any values are missing from the config use the current font's own values
function GlobalFontEditor:LoadFontsFromDB()
	for k,v in pairs(self.db.profile.fonts) do		
		if self:IsWidgetType(_G[k], "Font") then
			local dbFont = self.db.profile.fonts[k]			
			local kfont,ksize,kflags = _G[k]:GetFont()
			local font = LSM:Fetch(LSM.MediaType.FONT, dbFont.font, true) or kfont
			local size = dbFont.size or ksize
			local flags = self:ConcatFlags{
			   dbFont.outline and "OUTLINE" or dbFont.outline == nil and strfind(kflags, "OUTLINE") and "OUTLINE",
			   dbFont.thickoutline and "THICKOUTLINE" or dbFont.thickoutline == nil and strfind(kflags, "THICKOUTLINE") and "THICKOUTLINE",
			   dbFont.monochrome and "MONOCHROME" or dbFont.monochrome == nil and strfind(kflags, "MONOCHROME") and "MONOCHROME"
			}
			
			_G[k]:SetFont(font, size, flags)
		end
	end
end

--- Set a font back to blizzard's default setting
function GlobalFontEditor:ResetFont(font, iorder, group)		
		if self.fontDefaults[font] then
			self.db.profile.fonts[font] = nil
			self.db:RegisterDefaults(self.defaultConfig)
			
			local dbFont = self.fontDefaults[font]
			
			if self:IsWidgetType(_G[font], "Font") then				
				local f  = LSM:Fetch(LSM.MediaType.FONT, dbFont.font, true)
				local s  = dbFont.size
				local fl = self:ConcatFlags{
				   dbFont.outline and "OUTLINE",
				   dbFont.thickoutline and "THICKOUTLINE",
				   dbFont.monochrome and "MONOCHROME"
				}
				
				_G[font]:SetFont(f,s,fl)
			end			
		end	
		
	self:InitializeFontGroup(font, iorder, group)
end

--- Converts an array of flags to a string suitable for use in Font:SetFont()
--- Valid input for each flag: "OUTLINE", "THICKOUTLINE", "MONOCHROME", "", nil
--- Ignored values: "" and nil. Does not filter duplicates
function GlobalFontEditor:ConcatFlags(rawflags)
	local flags = {}	

	for i=1,#rawflags do
	   if rawflags[i] and rawflags[i] ~= "" then
		  flags[#flags + 1] = rawflags[i]
	   end
	end

	flags = strjoin(",", unpack(flags))	
	--Prevent crash on monochrome only *shrug*
	return flags ~= "MONOCHROME" and flags or nil
end

--- Determines if the argument 'var' is a widget of type 'wtype'
--- This is essentially a wrapper for UIObject:(Is/Get)ObjectType that also 
--- does some sanity checks
--- args:
---		var   - any lua value
---		btype - any lua value, nil/false = false, anything else = true
---			Passing true will check if 'var' inherits
---			'wtype', false or nil wil only check if 'var'
---			is explicitly of type 'wtype'
--- returns: 1 if true, nil otherwise (same convention as UIObject:IsObjectType)
function GlobalFontEditor:IsWidgetType(var, wtype, btype)    
    return (type(var) == "table" and
            type(var.GetObjectType) == "function" and
            type(var.IsObjectType) == "function" and
            (not btype or var:IsObjectType(wtype)) and
            (btype or var:GetObjectType() == wtype)) and 1 or nil
end

--- Reverse lookup (needed to determine LSM names for default fonts)
function LSM:GetKey(mediaType, value)
	for k,v in pairs(LSM:HashTable(mediaType)) do
		if v == value then return k end
	end	
	return nil
end
