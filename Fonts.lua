local GlobalFontEditor = LibStub("AceAddon-3.0"):GetAddon("GlobalFontEditor")

GlobalFontEditor.fonts = {
	["SystemFont_Tiny"] = {
		["GameFontBlackTiny"] = {},
		["GameFontWhiteTiny"] = {},
	},
	["SystemFont_Small"] = {
		["GameFontBlackSmall"] = {
			["GameFontWhiteSmall"] = {},
		},
		["AchievementDescriptionFont"] = {
			["AchievementCriteriaFont"] = {},
		},
	},
	["SystemFont_Outline_Small"] = {
		["TextStatusBarText"] = {},
	},
	["SystemFont_Outline"] = {
		["TextStatusBarTextLarge"] = {},
	},
	["SystemFont_Shadow_Small"] = {
		["GameFontNormalSmall"] = {
			["GameFontNormalSmallLeft"] = {},
			["GameFontHighlightSmall"] = {
				["GameFontHighlightSmallLeft"] = {
					["GameFontHighlightSmallLeftTop"] = {},
				},
				["GameFontHighlightSmallRight"] = {},
				["GameFontHighlightExtraSmall"] = {
					["GameFontHighlightExtraSmallLeft"] = {
						["GameFontHighlightExtraSmallLeftTop"] = {},
					},
				},
				["GameFontHighlightSmallOutline"] = {},
			},
			["GameFontDisableSmall"] = {
				["GameFontDisableSmallLeft"] = {},
			},
			["GameFontDarkGraySmall"] = {},
			["GameFontGreenSmall"] = {},
			["GameFontRedSmall"] = {},
		},
	},
	["SystemFont_InverseShadow_Small"] = {},
	["SystemFont_Med1"] = {
		["GameFontBlack"] = {
			["GameFontWhite"] = {},
			["QuestFontNormalSmall"] = {},
		},
	},
	["SystemFont_Shadow_Med1"] = {
		["GameFontNormal"] = {
			["GameFontNormalLeft"] = {
				["GameFontNormalLeftBottom"] = {},
				["GameFontNormalLeftGreen"] = {},
				["GameFontNormalLeftYellow"] = {},
				["GameFontNormalLeftOrange"] = {},
				["GameFontNormalLeftLightGreen"] = {},
				["GameFontNormalLeftGrey"] = {},
				["GameFontNormalLeftRed"] = {},
				["QuestDifficulty_Impossible"] = {},
				["QuestDifficulty_VeryDifficult"] = {},
				["QuestDifficulty_Difficult"] = {},
				["QuestDifficulty_Standard"] = {},
				["QuestDifficulty_Trivial"] = {},
				["QuestDifficulty_Header"] = {},
			},
			["GameFontHighlight"] = {
				["GameFontHighlightLeft"] = {},
				["GameFontHighlightCenter"] = {},
				["GameFontHighlightRight"] = {},
				["CombatLogFont"] = {},
			},
			["GameFontNormalRight"] = {},
			["GameFontNormalCenter"] = {},
			["GameFontDisable"] = {
				["GameFontDisableLeft"] = {},
			},
			["GameFontGreen"] = {},
			["GameFontRed"] = {},
		},
		["AchievementPointsFontSmall"] = {},
		["VehicleMenuBarStatusBarText"] = {},
	},
	["SystemFont_Med2"] = {
		["QuestFont"] = {
			["QuestFontLeft"] = {},
		},
	},
	["SystemFont_Shadow_Med2"] = {
		["FocusFontSmall"] = {},
	},
	["SystemFont_Med3"] = {
		["GameFontBlackMedium"] = {},
		["QuestFontHighlight"] = {},
	},
	["SystemFont_Shadow_Med3"] = {
		["GameFontNormalMed3"] = {},
		["GameFontHighlightMedium"] = {},
	},
	["SystemFont_Large"] = {
		["DialogButtonNormalText"] = {
			["DialogButtonHighlightText"] = {},
		},
	},
	["SystemFont_Shadow_Large"] = {
		["GameFontNormalLarge"] = {
			["GameFontNormalLargeLeft"] = {
				["GameFontNormalLargeLeftTop"] = {},
			},
			["GameFontHighlightLarge"] = {},
			["GameFontDisableLarge"] = {},
			["GameFontGreenLarge"] = {},
			["GameFontRedLarge"] = {},
			["ErrorFont"] = {},
		},
		["AchievementPointsFont"] = {},
	},
	["SystemFont_Huge1"] = {
		["GameFontNormalHugeBlack"] = {},
	},
	["SystemFont_Shadow_Huge1"] = {
		["GameFontNormalHuge"] = {
			["GameFontHighlightHuge"] = {},
		},
	},
	["SystemFont_OutlineThick_Huge2"] = {
		["PVPInfoTextFont"] = {},
	},
	["SystemFont_Shadow_Outline_Huge2"] = {
		["MovieSubtitleFont"] = {},
	},
	["SystemFont_Shadow_Huge3"] = {
		["BossEmoteNormalHuge"] = {},
		["CombatTextFont"] = {},
	},
	["SystemFont_OutlineThick_Huge4"] = {
		["SubZoneTextFont"] = {},
	},
	["SystemFont_OutlineThick_WTF"] = {
		["ZoneTextFont"] = {},
		["WorldMapTextFont"] = {},
	},
	["NumberFont_Shadow_Small"] = {
		["ChatFontSmall"] = {},
	},
	["NumberFont_OutlineThick_Mono_Small"] = {
		["NumberFontNormalSmall"] = {
			["NumberFontNormalSmallGray"] = {},
		},
	},
	["NumberFont_Shadow_Med"] = {
		["ChatFontNormal"] = {},
	},
	["NumberFont_Outline_Med"] = {
		["NumberFontNormal"] = {
			["NumberFontNormalRight"] = {
				["NumberFontNormalRightRed"] = {},
				["NumberFontNormalRightYellow"] = {},
			},
			["NumberFontNormalYellow"] = {},
		},
	},
	["NumberFont_Outline_Large"] = {
		["NumberFontNormalLarge"] = {
			["NumberFontNormalLargeRight"] = {
				["NumberFontNormalLargeRightRed"] = {},
				["NumberFontNormalLargeRightYellow"] = {},
			},
			["NumberFontNormalLargeYellow"] = {},
		},
	},
	["NumberFont_Outline_Huge"] = {
		["NumberFontNormalHuge"] = {},
	},
	["QuestFont_Large"] = {
		["ItemTextFontNormal"] = {},
	},
	["QuestFont_Shadow_Huge"] = {
		["QuestTitleFont"] = {},
		["QuestTitleFontBlackShadow"] = {},
	},
	["QuestFont_Shadow_Small"] = {},
	["GameTooltipHeader"] = {
		["GameTooltipHeaderText"] = {},
	},
	["MailFont_Large"] = {
		["MailTextFontNormal"] = {},
	},
	["SpellFont_Small"] = {
		["SubSpellFont"] = {},
	},
	["InvoiceFont_Med"] = {
		["InvoiceTextFontNormal"] = {},
	},
	["InvoiceFont_Small"] = {
		["InvoiceTextFontSmall"] = {},
	},
	["Tooltip_Med"] = {
		["GameTooltipText"] = {},
	},
	["Tooltip_Small"] = {
		["GameTooltipTextSmall"] = {},
	},
	["AchievementFont_Small"] = {
		["AchievementDateFont"] = {},
	},
	["ReputationDetailFont"] = {},
	["FriendsFont_Normal"] = {},
	["FriendsFont_Small"] = {},
	["FriendsFont_Large"] = {},
	["FriendsFont_UserText"] = {},
	["GameFont_Gigantic"] = {},
}

function GlobalFontEditor.kvpair(k,t)
   r = {}
   r[k] = t[k]
   return r
end

function GlobalFontEditor.keySort(t)
   a = {}
   for k in pairs(t) do table.insert(a, GlobalFontEditor.kvpair(k,t)) end
   table.sort(a, function(c1,c2) return tostring(next(c1)) < tostring(next(c2)) end)
   return a
end

GlobalFontEditor.fonts = GlobalFontEditor.keySort(GlobalFontEditor.fonts)
