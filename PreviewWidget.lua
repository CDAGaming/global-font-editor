-- Widget is based on the AceGUIWidget-DropDown.lua supplied with AceGUI-3.0
-- Widget created by Yssaril
local GlobalFontEditor = LibStub("AceAddon-3.0"):GetAddon("GlobalFontEditor")

function GlobalFontEditor:InitializePreviewWidget(font)
	local AceGUI = LibStub("AceGUI-3.0")	
	local AGSMW = LibStub("AceGUISharedMediaWidgets-1.0")

	do
		--DiscordGregory: This if block isn't in the original code, I put it here because 
		--I just wanted to get the addon going so I could see how it worked. If you remove it
		--there will be an error about nil fonts.
		if (not font) then
		else
			local widgetType = "FontPreview-"..font:GetName()
			local widgetVersion = 1
	
			local function OnAcquire(self)
				self:SetHeight(50)
				self:SetWidth(600)			
				
				fs = self.frame:CreateFontString(nil,"OVERLAY")
				fs:SetFontObject(font)
				fs:SetText(font:GetName())
				fs:SetPoint("CENTER", self.frame, "CENTER")
			end
	
			local function OnRelease(self)
				self.frame:ClearAllPoints()
				self.frame:Hide()
			end
			
			local function SetLabel(self, text) end
			local function SetText(self, text) end
			
	
			local function Constructor()
				local frame = CreateFrame("Frame", "FontPreview-"..font:GetName(), UIParent)
				local self = {}
	
				self.type = widgetType
				self.frame = frame
				frame.obj = self
	
				self.OnRelease = OnRelease
				self.OnAcquire = OnAcquire
				self.SetLabel = SetLabel
				self.SetText = SetText
	
				AceGUI:RegisterAsWidget(self)
				return self
			end
			AceGUI:RegisterWidgetType(widgetType, Constructor, widgetVersion)
		end
	end
end