﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml;
using System.Xml.Linq;

namespace LuaFontGen
{
    static class Program
    {
        private class TreeNode<T>
        {
            internal T Value
            {
                get;
                set;
            }

            private List<TreeNode<T>> children;
            internal List<TreeNode<T>> Children
            {
                get
                {
                    if (this.children == null)
                    {
                        this.children = new List<TreeNode<T>>();
                    }

                    return this.children;
                }
            }

            internal bool Contains(T key)
            {
                if (Value.Equals(key))
                {
                    return true;
                }
                else
                {
                    foreach (TreeNode<T> child in this.Children)
                    {
                        if (child.Contains(key))
                        {
                            return true;
                        }
                    }
                }

                return false;
            }

            internal TreeNode<T> Get(T key)
            {
                if (Value.Equals(key))
                {
                    return this;
                }
                else
                {
                    TreeNode<T> retval = null;

                    foreach (TreeNode<T> child in this.Children)
                    {
                        retval = child.Get(key);

                        if (retval != null)
                        {
                            return retval;
                        }
                    }
                }

                return null;
            }
        }

        private static IEnumerable<XElement> Elements(this XElement source, string localName, bool ignoreNS)
        {
            if (ignoreNS)
            {
                return source.Elements().Where(e => e.Name.LocalName == localName);
            }
            else
            {
                return source.Elements(localName);
            }
        }

        private class FontNode : TreeNode<string>
        {
            public string Name
            {
                get
                {
                    return this.Value;
                }

                set
                {
                    this.Value = value;
                }
            }

            public FontNode(string name = "", FontNode parent = null)
            {
                this.Name = name;
                this.Parent = parent;
            }

            private FontNode parent;
            public FontNode Parent
            {
                get
                {
                    return parent;
                }

                set
                {
                    parent = value;

                    if (parent != null)
                    {
                        parent.Children.Add(this);
                    }
                }
            }
        }

        static void Main(string[] args)
        {            
            FontNode root = new FontNode();

            ParseFonts("Fonts.xml", root);
            ParseFonts("FontStyles.xml", root);

            FileStream f = File.Create("output.lua");
            StreamWriter writer = new StreamWriter(f);

            writer.Write(BuildOutput(root));
            writer.Flush();
            writer.Close();
        }

        static private String BuildOutput(FontNode node)
        {
            StringBuilder b = new StringBuilder();
            b.Append("fonts = {");

            foreach (FontNode child in node.Children)
            {
                BuildOutput(b, child, "\t");
            }

            b.Append("\n}");

            return b.ToString();
        }

        static private void BuildOutput(StringBuilder output, FontNode node, string indent)
        {
            if (node.Children.Count > 0)
            {
                output.Append("\n" + indent + "[\"" + node.Name + "\"] = {");
                foreach (FontNode f in node.Children)
                {
                    BuildOutput(output, f, indent + "\t");
                }
                output.Append("\n" + indent + "},");
            }
            else
            {
                output.Append("\n" + indent + "[\"" + node.Name + "\"] = {},");
            }
        }

        static private void ParseFonts(string file, FontNode fonts)
        {
            XElement ui = XElement.Load(XmlReader.Create(file));


            foreach (XElement font in ui.Elements("Font", true))
            {
                XAttribute name = font.Attribute("name");
                XAttribute parent = font.Attribute("inherits");

                if (parent != null)
                {
                    if (fonts.Contains(parent.Value))
                    {
                        new FontNode(name.Value, fonts.Get(parent.Value) as FontNode);
                    }
                    else
                    {
                        throw new InvalidOperationException("Tried to parse a font who's parent had not yet been defined");
                    }
                }
                else
                {
                    fonts.Children.Add(new FontNode(name.Value));
                }
            }
        }
    }
}
