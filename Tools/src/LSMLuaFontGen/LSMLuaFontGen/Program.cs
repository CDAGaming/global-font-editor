﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Windows.Media;
using System.Globalization;
using System.Reflection;

namespace LSMLuaFontGen
{
    static class Program
    {
        static Properties.Settings Settings = Properties.Settings.Default;

        static void Main(string[] args)
        {                 
            List<string> lsmRegistrationLines = new List<string>();
            String currentDir = Directory.GetCurrentDirectory();

            foreach (string font in Directory.GetFiles(Settings.AddonDirectory + Settings.FontsDirectory))
            {
                string absolute_path = Path.Combine(currentDir, font);
                string uriPath = Path.GetFullPath((new Uri(absolute_path)).LocalPath);
                
                GlyphTypeface f = new GlyphTypeface(new Uri(uriPath));
                CultureInfo info = CultureInfo.CurrentCulture;
                
                string ttfName = f.FamilyNames[info] + (f.FaceNames[info] != "Regular" ? " " + f.FaceNames[info] : "");
                string ttfPath = uriPath.Substring(uriPath.LastIndexOf("Interface"));
                lsmRegistrationLines.Add(GenerateLSMRegisterLine(ttfName, ttfPath));
            }
            
            File.WriteAllText(Settings.AddonDirectory + Settings.FontsRegistrationLuaFile, Settings.FontsRegistrationLuaFileHeader);
            File.AppendAllLines(Settings.AddonDirectory + Settings.FontsRegistrationLuaFile, lsmRegistrationLines);
            foreach (string s in lsmRegistrationLines) System.Console.WriteLine(s);            
        }

        private static string GenerateLSMRegisterLine(string fontName, string fontPath)
        {
            return Settings.RegistryMethodTemplate.Replace("{FontName}", fontName).Replace("{FontPath}", fontPath);            
        }     
    }
}
