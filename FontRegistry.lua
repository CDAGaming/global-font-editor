--- Concept lovingly borrowed from SharedMediaAdditonalFonts addon 
--- 
--- I have implemented a secondary method of font registration to
--- allow font loading for users who do not have SharedMediaAdditionalFonts loaded
local GlobalFontEditor = LibStub("AceAddon-3.0"):GetAddon("GlobalFontEditor")
local LSM = LibStub("LibSharedMedia-3.0", true)
local mediaType = LSM and LSM.MediaType.FONT or "font"

GlobalFontEditor.registry = { [mediaType] = {} }
local GFERegistry = GlobalFontEditor.registry

function GlobalFontEditor:Register(key, data, langmask)	
	if LSM then 
		LSM:Register(mediaType, key, data, langmask)
	end
	if not GFERegistry[mediaType] then 
		GFERegistry[mediaType] = {} 
	end	
	table.insert(GFERegistry[mediaType], { key, data, langmask})
end

function GlobalFontEditor.OnEvent(this, event, ...)
	if not LSM then
		LSM = LibStub("LibSharedMedia-3.0", true)
		if LSM then
			for m,t in pairs(GlobalFontEditor.registry) do
				for _,v in ipairs(t) do
					LSM:Register(m, v[1], v[2], v[3])
				end
			end
		end
	end
end

GlobalFontEditor.frame = CreateFrame("Frame")
GlobalFontEditor.frame:SetScript("OnEvent", GlobalFontEditor.OnEvent)
GlobalFontEditor.frame:RegisterEvent("ADDON_LOADED")