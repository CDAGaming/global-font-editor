﻿## Interface: 50300
## Name: Global Font Editor
## Title: Global Font Editor
## Author: Peter Bergeron
## SavedVariables: GlobalFontEditorDB
## Notes: Edit all the fonts... seriously
## X-Curse-Packaged-Version: v0.1b
## X-Curse-Project-Name: Global Font Editor
## X-Curse-Project-ID: global-font-editor
## X-Curse-Repository-ID: wow/global-font-editor/mainline

Embeds.xml

GlobalFontEditor.lua
Fonts.lua
PreviewWidget.lua
Options.lua

GlobalFontEditor.xml