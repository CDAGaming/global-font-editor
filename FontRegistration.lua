--- This file was automatically generated by Global Font Editor ---
local GlobalFontEditor = LibStub("AceAddon-3.0"):GetAddon("GlobalFontEditor")
local LSM = LibStub("LibSharedMedia-3.0", true)
local fontMediaType = LSM and LSM.MediaType.FONT or "font"

--- If SharedMediaAdditionalFonts is loaded then use that, otherwise use our own registry ---
local FontRegistry = SharedMediaAdditionalFonts or GlobalFontEditor