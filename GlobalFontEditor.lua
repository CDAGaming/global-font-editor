local GlobalFontEditor = LibStub("AceAddon-3.0"):NewAddon("GlobalFontEditor", "AceConsole-3.0", "AceEvent-3.0")
GFE = GlobalFontEditor

function GlobalFontEditor:OnInitialize()
	self:LoadSystemDefaults(self.fonts)	
end

function GlobalFontEditor:OnEnable()
	self:InitializeConfiguration()	
	self:LoadFontsFromDB()
	self:InitializeConfigurationGUI()
end

function GlobalFontEditor:ChatCommand()		
	InterfaceOptionsFrame_OpenToCategory(self.ConfigurationFrame)
end

function GlobalFontEditor:Reset()
	self:InitializeConfiguration()
	self:LoadFontsFromDB()
end

function GlobalFontEditor:Update()
	self:LoadFontsFromDB()
end

function GlobalFontEditor:FontStringIdentTooltip_MouseOverTest(tip)
	local LSM = LibStub("LibSharedMedia-3.0")
	local matches = {}
	for k,v in pairs(_G) do
      if type(v) == "table" and type(v.GetObjectType) == "function" and v:GetObjectType() == "FontString" then
         if v:IsMouseOver() and v:IsVisible() then matches[#matches+1] = v end
      end   
	end
	local prefix = "FontStringIdentTooltipTextLeft"
	local hfont, font, layer = "GameTooltipHeaderText", "GameTooltipText", "ARTWORK"
	local lines = {}
	lines[#lines+1] = "Mouseover Fonts:"
   
	--Just in case multiple font strings match
	for i=1, #matches do
		local indent = "   "
		if #matches > 1 then			
			lines[#lines+1] = ident.."Match "..tostring(i)..":"
			indent = indent.."   "
		end
		do
			lines[#lines+1] = indent.."  Name: "..matches[i]:GetName()
			lines[#lines+1] = indent.."  Font: "..(LSM:GetKey(LSM.MediaType.FONT, matches[i]:GetFont()) or matches[i]:GetFont())
		end
		if matches[i]:GetFontObject() then
			lines[#lines+1] = indent.."Parent: "..matches[i]:GetFontObject():GetName()
			lines[#lines+1] = indent.."  Root: "..(function() 
				local s,tmp = "",matches[i]:GetFontObject()				
				while tmp do
					s = tmp:GetName()
					tmp = tmp:GetFontObject()
				end
				return s
			end)()
		end			
	end	
	
	tip:ClearLines()
	for i=1, #lines do		
		tip:AddLine(lines[i])
	end
	tip:Show()
end

function GlobalFontEditor:FontStringIdentTooltip_OnLoad(tip)
	tip:SetFrameLevel(tip:GetFrameLevel() + 2)
	tip:SetBackdropBorderColor(TOOLTIP_DEFAULT_COLOR.r, TOOLTIP_DEFAULT_COLOR.g, TOOLTIP_DEFAULT_COLOR.b)
	tip:SetBackdropColor(TOOLTIP_DEFAULT_BACKGROUND_COLOR.r, TOOLTIP_DEFAULT_BACKGROUND_COLOR.g, TOOLTIP_DEFAULT_BACKGROUND_COLOR.b)
	tip.statusBar2 = getglobal(tip:GetName().."StatusBar2")
	tip.statusBar2Text = getglobal(tip:GetName().."StatusBar2Text")
end

function GlobalFontEditor:FontStringIdentTooltip_Toggle()
	local tip = _G["FontStringIdentTooltip"]
	if tip:IsVisible() then
		tip:Hide()
	else
		tip:SetOwner(UIParent, "ANCHOR_NONE")
		tip:SetPoint("BOTTOMRIGHT", UIParent, "BOTTOMRIGHT", -CONTAINER_OFFSET_X - 13, CONTAINER_OFFSET_Y)
		tip.default = 1	
		self:FontStringIdentTooltip_MouseOverTest(tip)
		tip:Show()
	end
end

local tooltipUpdateTime = .5
local timeSinceLast = 0
function GlobalFontEditor:FontStringIdentTooltip_OnUpdate(tip, elapsed)
	timeSinceLast = timeSinceLast - elapsed
	if ( timeSinceLast <= 0 ) then
		timeSinceLast = tooltipUpdateTime
		self:FontStringIdentTooltip_MouseOverTest(tip)
	end
end

function GlobalFontEditor:FontStringIdentTooltip_OnShow(tip)
	local parent = tip:GetParent() or UIParent
	local ps = parent:GetEffectiveScale()
	local px, py = parent:GetCenter()
	px, py = px * ps, py * ps;
	local x, y = GetCursorPosition()
	tip:ClearAllPoints()
	if (x > px) then
		if (y > py) then
			tip:SetPoint("BOTTOMLEFT", parent, "BOTTOMLEFT", 20, 20)
		else
			tip:SetPoint("TOPLEFT", parent, "TOPLEFT", 20, -20)
		end
	else
		if (y > py) then
			tip:SetPoint("BOTTOMRIGHT", parent, "BOTTOMRIGHT", -20, 20)
		else
			tip:SetPoint("TOPRIGHT", parent, "TOPRIGHT", -20, -20)
		end
	end
end

GlobalFontEditor.FontStringIdentTooltip_OnEnter = GlobalFontEditor.FontStringIdentTooltip_OnShow